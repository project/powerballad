
This song was initially written by Kitten Killers and belongs to the Drupal community.
Music and lyrics are GPL. Feel free to use, study, share and improve. Don't know where to start? Have a look in the issue queue!

For a discussion about why people write silly Drupal songs, see http://nodeone.se/node/562


Thanks to all people engaged in this project!


Patchers, hard-working testers and invaluable support (in alphabetical order):
* chx
* Cyberschorsch
* freudenreichmedia
* pvasili
* SeanBannister
* ximo


Kitten Killers are:
* Meow Gutter (screams and shrieks)
* Axl Gore => MickeA (double-explosive guitars)
* Claw Chords (lead guitar)
* Dick the Ripper => dixon_ (tinnitus drums)
* Slash Falcon => Itangalo (skull-cracking bass)
* Psycho Whiskers => bobodrone (keyboard)